import com.devcamp.jbr1s50.Account;

public class App {
    public static void main(String[] args) throws Exception {
       
        Account account1 = new Account("ABEF123987", "Truong Kim Huong");
        Account account2 = new Account("ghta128527", "Huynh Minh Duy", 1000);

        System.out.println(account1);
        System.out.println(account2);
        // subtask 4: tài khoản 1 thêm 2000 và cho tài khoản 2 thêm 3000
        account1.credit(20000);
        account2.credit(30000);

        System.out.println("account1 sau khi tang 20000: " + account1);
        System.out.println("account1 sau khi tang 30000: "+ account2);

        // subtask 5: giảm tài khoản 1 đi 1000 và tài khoản 2 đi 5000
        account1.debit(1000);
        account2.debit(5000);

        System.out.println("account1 sau khi giam 1000: " + account1);
        System.out.println("account2 sau khi giam 5000: "+ account2);

        // subtask6: chuyển 2000 từ tài khoản 1 đến tài khoản 2
        account1.tranfersTo(account2, 2000);
    
        System.out.println("account1 sau khi chuyen 2000 cho account2: " + account1);
        System.out.println("account2 sau khi nhan 2000 tu account1: "+ account2);

        //subtask7: chuyển 2000 từ tài khoản 2 đến tài khoản 1
        account2.tranfersTo(account1, 2000);
    
        System.out.println("account2 sau khi chuyen 2000 cho account: " + account2);
        System.out.println("account1 sau khi nhan 2000 tu account2: "+ account1);
    }
}
