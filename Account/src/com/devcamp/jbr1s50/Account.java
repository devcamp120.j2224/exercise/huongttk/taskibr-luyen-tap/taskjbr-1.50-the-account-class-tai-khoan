package com.devcamp.jbr1s50;

public class Account {

    private String id = "";
    private String name = "";
    private int balance = 0;

    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getBalance() {
        return balance;
    }
    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int credit(int amount){
        this.balance = this.balance + amount;
        return this.balance;
    }
    
    public int debit(int amount) {
        if (amount <= this.balance) {
            this.balance = this.balance - amount;
        } else {
            System.out.println("Amount exceeed balance");
        }
        return balance;
    }
    public void tranfersTo(Account another, int amount) {
        if (amount <= this.balance) {
            this.debit(amount);
            another.credit(amount);
        } else {
            System.out.println("Amount exceeed balance");
        }

    }
    @Override
    public String toString() {
        return "Account [ ID = " + id + ", name = " + name + ", balance = " + balance  + " ]";
    }
   
}
